package mis.pruebas.springbootapp2.modelo;

public class DatosPedido {
    public String idProveedor;
    public long idPedido;
    public String proveedor;
    public String producto;
    public double cantidad;
    public int estado;
}
