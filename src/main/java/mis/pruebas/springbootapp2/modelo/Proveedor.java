package mis.pruebas.springbootapp2.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;

public class Proveedor {
    @JsonIgnore public String id;

    public String nombre;
    public String direccion;

    @JsonIgnore
    public ArrayList<Pedido> pedidos = new ArrayList<>();
}
