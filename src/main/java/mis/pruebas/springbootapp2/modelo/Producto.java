package mis.pruebas.springbootapp2.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

// POJO - Plain Old Java Object
public class Producto {
    @JsonIgnore
    @Id public String id; // -> _id

    public String nombre;
    public double precio;
    public double cantidad;

    // Jackson - Serializador/Deserializador JSON usado por Spring.
    public List<String> idsProveedores = new ArrayList<>();
}
