package mis.pruebas.springbootapp2.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Pedido {
    @JsonIgnore
    public long id;

    public String producto;
    public double cantidad;

    public int estado;


    public String toString() {
        return String.format("Pedido id: %d producto: '%s' cantidad: %f, estado: %d",
                this.id, this.producto, this.cantidad, this.estado);
    }
}
