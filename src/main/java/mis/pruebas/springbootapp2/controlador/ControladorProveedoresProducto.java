package mis.pruebas.springbootapp2.controlador;


import mis.pruebas.springbootapp2.modelo.Producto;
import mis.pruebas.springbootapp2.modelo.Proveedor;
import mis.pruebas.springbootapp2.servicio.ServicioProducto;
import mis.pruebas.springbootapp2.servicio.ServicioProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(RutasApi.PROVEEDORES_PRODUCTO)
@CrossOrigin
public class ControladorProveedoresProducto {

    // HAL JSON

    // GET [], POST *

    @Autowired
    ServicioProducto servicioProducto;

    @Autowired
    ServicioProveedor servicioProveedor;


    public static class ProveedorProducto {
        public String nombre;
        public ProveedorProducto(String nombre) {
            this.nombre = nombre;
        }
    }

    @GetMapping
    public CollectionModel<EntityModel<ProveedorProducto>> obtenerProveedoresProducto(@PathVariable String idProducto) {

        final Producto p = obtenerProductoPorId(idProducto);
        final List<String> idsProveedores = p.idsProveedores;

        return CollectionModel.of(
                idsProveedores.stream().map(
                        idEsteProveedor ->
                                EntityModel.of(new ProveedorProducto("")).add(Arrays.asList(
                                    linkTo(methodOn(ControladorProveedor.class).obtenerProveedor(idEsteProveedor)).withRel("proveedor").withTitle("Proveedor de este producto")
                        ))).collect(Collectors.toUnmodifiableList()))
                .add(Arrays.asList(
                        linkTo(methodOn(this.getClass()).obtenerProveedoresProducto(idProducto)).withSelfRel()
                ));
    }

    public static class ProveedorEntrada {
        public String idProveedor;
    }

    @PutMapping
    public void agregarReferenciaProveedorProducto(@PathVariable String idProducto,
                                                   @RequestBody ProveedorEntrada p) {

        final Producto x = obtenerProductoPorId(idProducto);

        final Proveedor v = this.servicioProveedor.obtenerProveedor(p.idProveedor);
        if(v == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        for(String id: x.idsProveedores) {
            if(id.equalsIgnoreCase(p.idProveedor))
                return; // 200 OK
        }

        x.idsProveedores.add(p.idProveedor);
        this.servicioProducto.reemplazarProducto(idProducto, x);
    }


    @DeleteMapping("/{idProveedor}")
    public void borrarReferenciaProveedorProducto(@PathVariable String idProducto,
                                                  @PathVariable String idProveedor) {

        final Producto x = obtenerProductoPorId(idProducto);

        // buscar índice del idProveedor en la lista de idsProveedores del producto idProducto.
        boolean encontrado = false;
        int j = 0; // índice encontrado.
        for(int i = 0; i < x.idsProveedores.size(); i++) {
            if(x.idsProveedores.get(i).equalsIgnoreCase(idProveedor)) {
                j = i;
                encontrado = true;
            }
        }
        if(encontrado) {
            // Si lo encuentro.
            x.idsProveedores.remove(j);
            this.servicioProducto.reemplazarProducto(idProducto, x);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    public Producto obtenerProductoPorId(String idProducto) {
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

}
