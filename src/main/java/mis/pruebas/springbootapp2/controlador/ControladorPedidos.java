package mis.pruebas.springbootapp2.controlador;

import mis.pruebas.springbootapp2.modelo.DatosPedido;
import mis.pruebas.springbootapp2.modelo.Pedido;
import mis.pruebas.springbootapp2.modelo.Proveedor;
import mis.pruebas.springbootapp2.servicio.ServicioProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/stocks/v1/pedidos")
public class ControladorPedidos {

    @Autowired
    ServicioProveedor servicioProveedor;


    @GetMapping
    public List<DatosPedido> obtenerPedidos(
            @RequestParam(required = false) String producto,
            @RequestParam(required = false) Integer estado) {
        return this.servicioProveedor.obtenerPedidos(producto, estado);
    }

}
