package mis.pruebas.springbootapp2.controlador;

import mis.pruebas.springbootapp2.seguridad.JwtBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/autent/v1/token")
public class ControladorAutenticacion {

    public  static class Aut {
        public String userId;
        public String passwd;
    }

    @Autowired
    JwtBuilder jwtBuilder;

    @GetMapping
    public String obtenerToken(@RequestBody Aut aut) {
        if(!"desarrollo".equalsIgnoreCase(aut.userId)
        || !"password1234".equalsIgnoreCase(aut.passwd)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }

        return jwtBuilder.generateToken(aut.userId, "pruebas");
    }
}
