package mis.pruebas.springbootapp2.controlador;

import mis.pruebas.springbootapp2.modelo.Producto;
import mis.pruebas.springbootapp2.seguridad.JwtBuilder;
import mis.pruebas.springbootapp2.servicio.ServicioProducto;
import org.jose4j.jwt.JwtClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(RutasApi.PRODUCTOS)
@ExposesResourceFor(Producto.class)
@CrossOrigin
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @Autowired
    EntityLinks entityLinks;

    @Autowired
    JwtBuilder jwtBuilder;


    @PostMapping
    public ResponseEntity agregarProducto(@RequestBody Producto p) {
        // 200 OK
        // Location: http://localhost:9999/sotcks/v1/productos/99

        this.servicioProducto.agregar(p);

        return ResponseEntity
                .ok()
                .location(obtenerUrlProducto(p).toUri())
                .build()
                ;
    }

    /*
    Link { url, rel }
    EntityModel<T> T=Producto
    CollectionModel<T> T=EntityModel<Producto>
    */

    public static class ResumenProducto {
        public String nombre;
        public double precio;
    }

    @GetMapping
    public CollectionModel<EntityModel<ResumenProducto>> obtenerProductos(
            @RequestHeader("Authorization") String authorization,
            @RequestParam(required = false) Double minPrecio,
            @RequestParam(required = false) Double maxPrecio
    ) {
        //validarToken(authorization);

        List<Producto> pp = null;

        if(minPrecio != null && maxPrecio != null) {
            pp = this.servicioProducto.obtenerPorRangoPrecio(minPrecio.doubleValue(), maxPrecio.doubleValue());
        } else {
            pp = this.servicioProducto.obtenerProductos();
        }

        return CollectionModel.of(
                pp.stream().map(p -> crearRespuestaResumenProducto(p)).collect(Collectors.toList())
        ).add(linkTo(methodOn(this.getClass()).obtenerProductos(null, null, null)).withSelfRel());
    }

    private void validarToken(String authorization) {

        // Sanear la entrada.
        authorization = authorization == null ? "" : authorization.trim();

        // TODO: validar el token.
        System.out.println("Authorization: " + authorization);

        if(!authorization.startsWith("Bearer "))
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

        final String token = authorization.substring(7).trim();


        try {
            final JwtClaims claims = jwtBuilder.generateParseToken(token);

            // JWT - JSON Web Token {}
            if (!claims.hasClaim("userID"))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);

            System.out.println("AUTENTICACION: userID = " + claims.getClaimValue("userID"));

        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    private Link obtenerUrlProducto(Producto p) {
        return this.entityLinks
                .linkToItemResource(p.getClass(), p.id)
                .withSelfRel()
                .withTitle("Ver detalles de este producto")
                ;
    }

    private EntityModel<ResumenProducto> crearRespuestaResumenProducto(Producto p) {
        final ResumenProducto r = new ResumenProducto();
        r.nombre = p.nombre;
        r.precio = p.precio;
        return EntityModel.of(r).add(obtenerUrlProducto(p));
    }

    // ------------------------------------------------------------

    private List<Link> crearEnlacesProducto(Producto p) {

        // HTTP: GET  http://localhost:9999/stocks/v1/productos -> SPRING: @GetMapping obtenerProductos()
        // @GetMapping obtenerProductos() -> http://localhost:9999/stocks/v1/productos

        final Link productos = linkTo(methodOn(this.getClass()).obtenerProductos(null, null, null))
                .withRel("productos").withTitle("Lista de todos los productos");

        final Link proveedores = linkTo(methodOn(ControladorProveedoresProducto.class)
                    .obtenerProveedoresProducto(p.id))
                .withRel("proveedores")
                .withTitle("Lista de proveedores");

        return Arrays.asList(
                obtenerUrlProducto(p),
                productos,
                proveedores
        );
    }

    private EntityModel<Producto> crearRespuestaProducto(Producto p) {
        return EntityModel.of(p).add(crearEnlacesProducto(p));
    }

    @GetMapping("/{idProducto}")
    public EntityModel<Producto> obtenerProducto(@PathVariable String idProducto) {

        // HTTP: GET  http://localhost:9999/stocks/v1/productos/{idProducto} -> SPRING: @GetMapping obtenerProducto(@PathVariable idProducto)

        final Producto p = buscarProductoPorId(idProducto);

        final EntityModel<Producto> e = EntityModel.of(p);

        // @GetMapping obtenerProducto(@PathVariable idProducto) -> http://localhost:9999/stocks/v1/productos/{idProducto}

        //final Link enlaceEsteProducto = this.entityLinks.linkToItemResource(Producto.class, idProducto).withSelfRel();
        final Link enlaceEsteProducto = linkTo(methodOn(this.getClass()).obtenerProducto(idProducto)).withSelfRel();

        final Link enlaceProveedoresProducto = linkTo(methodOn(ControladorProveedoresProducto.class).obtenerProveedoresProducto(idProducto))
                .withRel("proveedores").withTitle("Lista de proveedores de este producto");

        final Link enlaceTodosLosProductos = linkTo(methodOn(this.getClass()).obtenerProductos(null, null, null))
                .withRel("productos").withTitle("Todos los productos");


        final List<Link> listaEnlaces = Arrays.asList(
                enlaceEsteProducto,
                enlaceProveedoresProducto,
                enlaceTodosLosProductos
        );

        e.add(listaEnlaces);
        return e;
    }


    @DeleteMapping("/{idProducto}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProducto(@PathVariable String idProducto) {
        this.servicioProducto.borrarProducto(idProducto);
    }

    public static class ProductoEntrada {
        public String nombre;
        public Double precio;
        public Double cantidad;
    }

    @PutMapping("/{idProducto}")
    public void reemplazarProducto(@PathVariable(name = "idProducto") String id,
                                   @RequestBody ProductoEntrada p) {

        final Producto x = buscarProductoPorId(id); // Si no existe, lanza excepcion 404.

        // Controlar que vengan todos los campos con valores verosímiles.
        if(p.nombre == null ||  p.nombre.trim().isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(p.precio == null || p.precio <= 0.0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        if(p.cantidad == null || p.cantidad < 0.0)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        x.nombre = p.nombre;
        x.precio = p.precio;
        x.cantidad = p.cantidad;

        this.servicioProducto.reemplazarProducto(id, x);
    }

    @PatchMapping("/{idProducto}")
    public void modificarProducto(@PathVariable String idProducto,
                                  @RequestBody ProductoEntrada p) {

        if(!this.servicioProducto.existeProducto(idProducto))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        // Validación de los datos de entrada. SIEMPRE!!!
        if(p.nombre != null) {
            if(p.nombre.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if(p.precio != null) {
            if(p.precio <= 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if(p.cantidad != null) {
            if(p.cantidad < 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        this.servicioProducto.emparcharProducto(idProducto, p.nombre, p.precio, p.cantidad);
    }

    private Producto buscarProductoPorId(String idProducto) {
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }
}
