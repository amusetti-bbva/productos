package mis.pruebas.springbootapp2.controlador;

import mis.pruebas.springbootapp2.modelo.Proveedor;
import mis.pruebas.springbootapp2.servicio.ServicioProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(RutasApi.PROVEEDORES)
@CrossOrigin
public class ControladorProveedor {

    @Autowired
    ServicioProveedor servicioProveedor;

    @PostMapping
    public ResponseEntity agregarProveedor(@RequestBody Proveedor p){
        this.servicioProveedor.agregar(p);

        final Link enlaceEsteProveedor = linkTo(methodOn(this.getClass()).obtenerProveedor(p.id))
                .withSelfRel();

        return ResponseEntity
                .ok()
                .location(enlaceEsteProveedor.toUri())
                .build();
    }

    @GetMapping
    public CollectionModel<EntityModel<Proveedor>> obtenerProveedores(){
        final List<Proveedor> pp = this.servicioProveedor.obtenerProveedores();

        return CollectionModel.of(
                pp.stream().map(
                        p -> EntityModel.of(p)
                                .add(Arrays.asList(
                                        linkTo(methodOn(this.getClass()).obtenerProveedor(p.id)).withSelfRel(),
                                        linkTo(methodOn(this.getClass()).obtenerProveedores()).withRel("proveedores").withTitle("Todos los proveedores")
                                ))
                ).collect(Collectors.toUnmodifiableList())

        ).add(linkTo(methodOn(this.getClass()).obtenerProveedores()).withSelfRel());

    }



    // ----------------------------------------------------------------------

    @GetMapping("/{idProveedor}")
    public EntityModel<Proveedor> obtenerProveedor(@PathVariable String idProveedor){
        final Proveedor p = buscarProveedorPorId(idProveedor);
        return EntityModel.of(p).add(Arrays.asList(
                linkTo(methodOn(this.getClass()).obtenerProveedor(idProveedor)).withSelfRel(),
                linkTo(methodOn(this.getClass()).obtenerProveedores()).withRel("proveedores").withTitle("Todos los proveedores")
        ));
    }

    @PutMapping("/{idProveedor}")
    public void reemplazarProveedor(@PathVariable(name = "idProveedor") String id,
                                   @RequestBody Proveedor p){
        buscarProveedorPorId(id);
        p.id = id;
        this.servicioProveedor.reemplazarProveedor(id,p);
    }

    @DeleteMapping("/{idProveedor}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProveedor(@PathVariable(name = "idProveedor") String id ){
        this.servicioProveedor.eliminarProveedor(id);
    }

    private Proveedor buscarProveedorPorId(String idProveedor) {
        final Proveedor p = this.servicioProveedor.obtenerProveedor(idProveedor);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }
}
