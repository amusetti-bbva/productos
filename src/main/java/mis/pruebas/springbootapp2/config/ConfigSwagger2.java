package mis.pruebas.springbootapp2.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class ConfigSwagger2 {
    @Bean
    public Docket apiDocs() {
        return new Docket(DocumentationType.SWAGGER_2);
    }


    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        //        config.setAllowCredentials(...);
        //        config.addAllowedOrigin(...);
        //        config.setAllowedOriginPatterns(...);
        //        config.addAllowedHeader(...);
        //        config.addAllowedMethod(...);
        config.applyPermitDefaultValues();

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/v2/api-docs", config);
        return new CorsFilter(source);
    }

}
