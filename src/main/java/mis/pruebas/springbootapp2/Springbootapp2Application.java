package mis.pruebas.springbootapp2;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.ForwardedHeaderFilter;

import java.util.Arrays;

@SpringBootApplication
public class Springbootapp2Application {
	public static void main(String[] args) {
		SpringApplication.run(Springbootapp2Application.class, args);
	}

	// Solicitud HTTP -> [Filtro -> Filtro -> ForwardedHeaderFilter -> Filtro -> ... ] -> Respuesta HTTP
	@Bean
	public ForwardedHeaderFilter forwardedHeaderFilter() {
		// X-Forwarded-Host
		// X-Forwarded-Port
		// X-Forwarded-Proto
		return  new ForwardedHeaderFilter();
	}

	@Bean
	public CommandLineRunner lineaComando(ApplicationContext app) {
		return args -> {
			final String[] beans = app.getBeanDefinitionNames();
			System.out.println("#BEANS: " + app.getBeanDefinitionCount());
			Arrays.sort(beans);
			for(String b: beans) {
				System.out.println("BEAN: " + b);
			}
		};
	}
}
