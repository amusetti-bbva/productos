package mis.pruebas.springbootapp2.servicio.repos;

import mis.pruebas.springbootapp2.modelo.DatosPedido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

import java.util.List;
import java.util.Map;

public class RepositorioProveedorPersonalizadoImpl implements RepositorioProveedorPersonalizado {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<DatosPedido> obtenerPedidos(String producto, Integer estado) {

        System.out.println(String.format("===== %s.obtenerPedidos('%s', %d)", this.getClass().getName(), producto, estado));

        return this.mongoTemplate.aggregate(newAggregation(

                match(Criteria.where("pedidos.producto").is(producto).and("pedidos.estado").is(estado))

                , unwind("$pedidos")

                , match(Criteria.where("pedidos.producto").is(producto).and("pedidos.estado").is(estado))

                // CONJUNTO FINAL DE ACA PARA ADELANTE.

                , replaceRoot(ObjectOperators.MergeObjects.merge(
                        // k -> v
                        Map.of("idProveedor", "$_id",
                                "proveedor", "$nombre",
                                "idPedido" , "$pedidos._id",
                                "producto" , "$pedidos.producto",
                                "cantidad" , "$pedidos.cantidad")
                ))

        ), "proveedor", DatosPedido.class).getMappedResults();
    }

}
