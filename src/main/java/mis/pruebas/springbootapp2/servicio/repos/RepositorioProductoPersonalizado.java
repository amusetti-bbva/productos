package mis.pruebas.springbootapp2.servicio.repos;

import mis.pruebas.springbootapp2.modelo.Producto;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioProductoPersonalizado {
    public List<Producto> buscarPorRangoPrecioComplicado(double min, double max);
    public void emparcharProducto(String idProducto, String nombre, Double precio, Double cantidad);

    public boolean existeProducto(String idProducto);
}
