package mis.pruebas.springbootapp2.servicio.repos;

import mis.pruebas.springbootapp2.modelo.Proveedor;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioProveedor extends MongoRepository<Proveedor, String>,
    RepositorioProveedorPersonalizado {
}
