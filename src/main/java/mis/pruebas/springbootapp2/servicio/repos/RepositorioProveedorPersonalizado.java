package mis.pruebas.springbootapp2.servicio.repos;

import mis.pruebas.springbootapp2.modelo.DatosPedido;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioProveedorPersonalizado {

    public List<DatosPedido> obtenerPedidos(String producto, Integer estado);
}
