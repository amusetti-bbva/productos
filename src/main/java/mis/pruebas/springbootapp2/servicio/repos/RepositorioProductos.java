package mis.pruebas.springbootapp2.servicio.repos;

import mis.pruebas.springbootapp2.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioProductos extends MongoRepository<Producto, String>,
    RepositorioProductoPersonalizado {

    public List<Producto> findByPrecioBetween(double min, double max);

    @Query("{  $and : [  { 'precio' : { $gte : ?0 } }  , { 'precio' : { $lte : ?1 }}     ]   }")
    public List<Producto> buscarPorRangoPrecio(double min, double max);
}
