package mis.pruebas.springbootapp2.servicio;

import mis.pruebas.springbootapp2.modelo.Producto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ServicioProducto {

    public boolean existeProducto(String idProducto);

    public String agregar(Producto p);
    public List<Producto> obtenerProductos();
    public Producto obtenerProducto(String id);
    public void reemplazarProducto(String id, Producto p);
    public void borrarProducto(String id);

    public List<Producto> obtenerPorRangoPrecio(double minPrecio, double maxPrecio);

    public void emparcharProducto(String idProducto, String nombre, Double precio, Double cantidad);




}
