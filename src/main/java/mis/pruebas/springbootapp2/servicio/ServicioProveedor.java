package mis.pruebas.springbootapp2.servicio;

import mis.pruebas.springbootapp2.modelo.DatosPedido;
import mis.pruebas.springbootapp2.modelo.Proveedor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public interface ServicioProveedor {
    public String agregar(Proveedor p);

    public List<Proveedor> obtenerProveedores();

    public Proveedor obtenerProveedor(String id);

    public void reemplazarProveedor(String id, Proveedor p);

    public void eliminarProveedor(String id);

    public List<DatosPedido> obtenerPedidos(String producto, Integer estado);

}
