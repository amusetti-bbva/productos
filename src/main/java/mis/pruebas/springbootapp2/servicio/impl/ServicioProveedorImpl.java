package mis.pruebas.springbootapp2.servicio.impl;

import mis.pruebas.springbootapp2.modelo.DatosPedido;
import mis.pruebas.springbootapp2.modelo.Proveedor;
import mis.pruebas.springbootapp2.servicio.ServicioProveedor;
import mis.pruebas.springbootapp2.servicio.repos.RepositorioProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioProveedorImpl implements ServicioProveedor {

    @Autowired
    RepositorioProveedor repositorioProveedor;

    @Override
    public String agregar(Proveedor p) {
        return this.repositorioProveedor.save(p).id;
    }

    @Override
    public List<Proveedor> obtenerProveedores() {
        return this.repositorioProveedor.findAll();
    }

    @Override
    public Proveedor obtenerProveedor(String id) {
        final Optional<Proveedor> p = this.repositorioProveedor.findById(id);
        return p.isPresent() ? p.get() : null;
    }

    @Override
    public void reemplazarProveedor(String id, Proveedor p) {
        p.id = id;
        this.repositorioProveedor.save(p);
    }

    @Override
    public void eliminarProveedor(String id) {
        this.repositorioProveedor.deleteById(id);
    }

    @Override
    public List<DatosPedido> obtenerPedidos(String producto, Integer estado) {
        return this.repositorioProveedor.obtenerPedidos(producto, estado);
    }
}
