package mis.pruebas.springbootapp2.servicio.impl;

import mis.pruebas.springbootapp2.modelo.Producto;
import mis.pruebas.springbootapp2.servicio.ServicioProducto;
import mis.pruebas.springbootapp2.servicio.repos.RepositorioProductos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    @Autowired
    RepositorioProductos repositorioProductos;

    @Override
    public String agregar(Producto p) {
        this.repositorioProductos.insert(p);
        return p.id;
    }

    public boolean existeProducto(String idProducto) {
        return this.repositorioProductos.existeProducto(idProducto);
    }

    @Override
    public List<Producto> obtenerProductos() {
        return this.repositorioProductos.findAll();
    }

    @Override
    public Producto obtenerProducto(String id) {
        final Optional<Producto> p = this.repositorioProductos.findById(id);
        return p.isPresent() ? p.get() : null;
    }

    @Override
    public void reemplazarProducto(String id, Producto p) {
        p.id = id;
        this.repositorioProductos.save(p);
    }

    @Override
    public void emparcharProducto(String idProducto, String nombre, Double precio, Double cantidad) {
        this.repositorioProductos.emparcharProducto(idProducto, nombre, precio, cantidad);
    }

    @Override
    public void borrarProducto(String id) {
        this.repositorioProductos.deleteById(id);
    }

    public List<Producto> obtenerPorRangoPrecio(double minPrecio, double maxPrecio) {
        //return this.repositorioProductos.findByPrecioBetween(minPrecio, maxPrecio);
        //return this.repositorioProductos.buscarPorRangoPrecio(minPrecio, maxPrecio);
        return this.repositorioProductos.buscarPorRangoPrecioComplicado(minPrecio, maxPrecio);
    }
}
